/******************************************************************************
* Empty Clip
* Copyright (C) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

// Libraries
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <string>
#include <vector>

// Enumerations
enum EventType {
	EVENT_DOOR,
	EVENT_WALLSWITCH,
	EVENT_SPAWN,
	EVENT_CHECK,
	EVENT_END,
	EVENT_TEXT,
	EVENT_SOUND,
	EVENT_FLOORSWITCH,
	EVENT_ENABLE,
	EVENT_TELEPORT,
	EVENT_LIGHT,
	EVENT_SECRET,
	EVENT_LAVA,
};

struct _EventTile {

	_EventTile() { }
	_EventTile(const glm::ivec2 &Coord, int Layer, int BlockID) : Coord(Coord), Layer(Layer), BlockID(BlockID) {}

	glm::ivec2 Coord{0};
	int Layer{0};
	int BlockID{0};
};

// Classes
class _Event {

	public:

		void Update(double FrameTime);

		void AddTile(_EventTile Tile);
		void RemoveTile(const std::vector<_EventTile>::iterator &Iterator) { Tiles.erase(Iterator); }
		void DeleteBlockID(int Layer, int Index);
		std::vector<_EventTile>::iterator FindTile(const glm::ivec2 &Position);
		void StartTimer() { Timer = 0; }
		void Decrement() { Level--; }
		bool TimerExpired() const { return Timer > ActivationPeriod; }

		void GetBounds(glm::vec4 &Bounds) { Bounds[0] = Start.x; Bounds[1] = Start.y; Bounds[2] = End.x + 1.0f; Bounds[3] = End.y + 1.0f; }

		std::vector<_EventTile> Tiles;
		std::string ItemID;
		std::string MonsterID;
		std::string ParticleID;
		std::string SoundID;
		glm::ivec2 Start{0};
		glm::ivec2 End{0};
		double Timer{0.0};
		double ActivationPeriod{0.0};
		int Type{0};
		int Active{false};
		int Level{0};
		int SpawnLevel{0};
		int SpawnMultiplier{1};
		bool Switched{false};
		bool IsBossSpawn{false};
		bool IsMonsterSpawn{false};
};
