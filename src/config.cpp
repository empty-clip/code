/******************************************************************************
* Empty Clip
* Copyright (C) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <config.h>
#include <ae/actions.h>
#include <ae/util.h>
#include <actiontype.h>
#include <constants.h>
#include <SDL_filesystem.h>
#include <fstream>
#include <sstream>

// Globals
_Config Config;

// Initializes the config system
void _Config::Init(const std::string &ConfigFile) {

	// Set names for actions
	ae::Actions.State.resize(Action::COUNT);
	ae::Actions.ResetState();
	ae::Actions.State[Action::GAME_LEFT].Name = "game_left";
	ae::Actions.State[Action::GAME_RIGHT].Name = "game_right";
	ae::Actions.State[Action::GAME_UP].Name = "game_up";
	ae::Actions.State[Action::GAME_DOWN].Name = "game_down";
	ae::Actions.State[Action::GAME_USE].Name = "game_use";
	ae::Actions.State[Action::GAME_SPRINT].Name = "game_sprint";
	ae::Actions.State[Action::GAME_MAP].Name = "game_map";
	ae::Actions.State[Action::GAME_FLASHLIGHT].Name = "game_flashlight";
	ae::Actions.State[Action::GAME_FIRE].Name = "game_fire";
	ae::Actions.State[Action::GAME_AIM].Name = "game_aim";
	ae::Actions.State[Action::GAME_MELEE].Name = "game_melee";
	ae::Actions.State[Action::GAME_RELOAD].Name = "game_reload";
	ae::Actions.State[Action::GAME_WEAPONSWITCH].Name = "game_weaponswitch";
	ae::Actions.State[Action::GAME_INVENTORY].Name = "game_inventory";
	ae::Actions.State[Action::GAME_SORTINVENTORY].Name = "game_sortinventory";
	ae::Actions.State[Action::GAME_MOREINFO].Name = "game_moreinfo";
	ae::Actions.State[Action::GAME_FILTERGEAR].Name = "game_filtergear";
	ae::Actions.State[Action::GAME_FILTERMODS].Name = "game_filtermods";
	ae::Actions.State[Action::GAME_SWITCHOUTFIT].Name = "game_switchoutfit";
	ae::Actions.State[Action::GAME_MINIMAP].Name = "game_minimap";
	ae::Actions.State[Action::MISC_CONSOLE].Name = "misc_console";
	ae::Actions.State[Action::MISC_MENU].Name = "misc_menu";
	ae::Actions.State[Action::MISC_DEBUG].Name = "misc_debug";

	// Create config path
	char *PrefPath = SDL_GetPrefPath("", "emptyclip");
	if(PrefPath) {
		ConfigPath = SavePath = PrefPath;
		SDL_free(PrefPath);
	}
	else {
		throw std::runtime_error(std::string(__func__) + " unable to create config path!");
	}

	ConfigFilePath = ConfigPath + ConfigFile;
	ae::MakeDirectory(ConfigPath);

	// Load defaults
	SetDefaults(false);

	// Load config
	Load();
}

// Closes the config system
void _Config::Close() {
}

// Set defaults
void _Config::SetDefaults(bool FromOptionsScreen) {

	Version = CONFIG_VERSION;
	WindowSize = DEFAULT_WINDOW_SIZE;
	MSAA = DEFAULT_MSAA;
	Anisotropy = DEFAULT_ANISOTROPY;
	if(!FromOptionsScreen)
		Fullscreen = DEFAULT_FULLSCREEN;
	Vsync = DEFAULT_VSYNC;
	MaxFPS = DEFAULT_MAXFPS;
	AudioEnabled = DEFAULT_AUDIOENABLED;

	WeaponFlashes = true;
	WallDecals = true;
	FloorDecals = true;
	ShowIcons = false;
	Tutorial = true;
	AutoEquip = true;
	AutoOrganize = true;

	SoundVolume = 1.0f;
	MusicVolume = 1.0f;

	Locale = "";

	if(!FromOptionsScreen)
		LoadDefaultInputBindings(false);
}

// Load default key bindings
void _Config::LoadDefaultInputBindings(bool IfNone) {

	// Clear mappings
	if(!IfNone) {
		for(int i = 0; i < ae::_Input::INPUT_COUNT; i++)
			ae::Actions.ClearMappings(i);
	}

	// Game
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_E, Action::GAME_UP, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_D, Action::GAME_DOWN, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_S, Action::GAME_LEFT, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_F, Action::GAME_RIGHT, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_A, Action::GAME_SPRINT, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_SPACE, Action::GAME_USE, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_C, Action::GAME_INVENTORY, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_Z, Action::GAME_SORTINVENTORY, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::MOUSE_BUTTON, 1, Action::GAME_FIRE, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::MOUSE_BUTTON, 3, Action::GAME_AIM, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::MOUSE_BUTTON, 4, Action::GAME_MELEE, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::MOUSE_BUTTON, 5, Action::GAME_MOREINFO, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_R, Action::GAME_RELOAD, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_W, Action::GAME_WEAPONSWITCH, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_TAB, Action::GAME_MAP, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_V, Action::GAME_FLASHLIGHT, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_1, Action::GAME_FILTERGEAR, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_2, Action::GAME_FILTERMODS, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_Q, Action::GAME_SWITCHOUTFIT, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_G, Action::GAME_MINIMAP, 1.0f, -1.0f, IfNone);

	// Misc
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_GRAVE, Action::MISC_CONSOLE, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_F1, Action::MISC_MENU, 1.0f, -1.0f, IfNone);
	ae::Actions.AddInputMap(0, ae::_Input::KEYBOARD, SDL_SCANCODE_F2, Action::MISC_DEBUG, 1.0f, -1.0f, IfNone);
}

// Load the config file
void _Config::Load() {

	// Open file
	std::ifstream File(ConfigFilePath.c_str());
	if(!File) {
		Save();
		return;
	}

	// Read data into map
	Map.clear();
	char Buffer[256];
	while(File) {
		File.getline(Buffer, 256);
		if(File.good()) {
			std::string Line(Buffer);
			size_t Pos = Line.find_first_of('=');
			if(Pos != std::string::npos) {
				std::string Field = Line.substr(0, Pos);
				std::string Value = Line.substr(Pos+1, Line.size());

				Map[Field].push_back(Value);
			}
		}
	}

	// Close
	File.close();

	// Read version
	int ReadVersion = 0;
	GetValue("version", ReadVersion);
	if(ReadVersion != Version) {
		std::string OldVersionPath = ConfigFilePath + "." + std::to_string(ReadVersion);
		std::remove(OldVersionPath.c_str());
		std::rename(ConfigFilePath.c_str(), OldVersionPath.c_str());
		Save();
		return;
	}

	// Read config
	GetValue("window_width", WindowSize.x);
	GetValue("window_height", WindowSize.y);
	GetValue("fullscreen", Fullscreen);
	GetValue("vsync", Vsync);
	GetValue("max_fps", MaxFPS);
	GetValue("anisotropy", Anisotropy);
	GetValue("msaa", MSAA);
	GetValue("audio_enabled", AudioEnabled);
	GetValue("weapon_flashes", WeaponFlashes);
	GetValue("wall_decals", WallDecals);
	GetValue("floor_decals", FloorDecals);
	GetValue("show_icons", ShowIcons);
	GetValue("tutorial", Tutorial);
	GetValue("autoequip", AutoEquip);
	GetValue("autoorganize", AutoOrganize);
	GetValue("sound_volume", SoundVolume);
	GetValue("music_volume", MusicVolume);
	GetValue("locale", Locale);

	// Clear bindings
	for(int i = 0; i < ae::_Input::INPUT_COUNT; i++)
		ae::Actions.ClearMappings(i);

	// Load bindings
	for(size_t i = 0; i < ae::Actions.State.size(); i++) {
		std::ostringstream Buffer;
		Buffer << "action_" << ae::Actions.State[i].Name;

		// Get list of inputs for each action
		const auto &Values = Map[Buffer.str()];
		for(auto &Iterator : Values) {

			// Parse input bind
			int Rank;
			int InputType;
			int Input;
			char Dummy;
			std::istringstream Stream(Iterator);
			Stream >> Rank >> Dummy >> InputType >> Dummy >> Input;
			ae::Actions.AddInputMap(Rank, InputType, Input, i, 1.0f, -1.0f, false);
		}
	}

	// Add missing bindings
	LoadDefaultInputBindings(true);
}

// Save variables to the config file
void _Config::Save() {

	// Open file
	std::ofstream File(ConfigFilePath.c_str());
	if(!File.is_open())
		return;

	// Write variables
	File << "version=" << Version << std::endl;
	File << "window_width=" << WindowSize.x << std::endl;
	File << "window_height=" << WindowSize.y << std::endl;
	File << "fullscreen=" << Fullscreen << std::endl;
	File << "vsync=" << Vsync << std::endl;
	File << "max_fps=" << MaxFPS << std::endl;
	File << "msaa=" << MSAA << std::endl;
	File << "anisotropy=" << Anisotropy << std::endl;
	File << "audio_enabled=" << AudioEnabled << std::endl;
	File << "weapon_flashes=" << WeaponFlashes << std::endl;
	File << "wall_decals=" << WallDecals << std::endl;
	File << "floor_decals=" << FloorDecals << std::endl;
	File << "show_icons=" << ShowIcons << std::endl;
	File << "tutorial=" << Tutorial << std::endl;
	File << "autoequip=" << AutoEquip << std::endl;
	File << "autoorganize=" << AutoOrganize << std::endl;
	File << "sound_volume=" << SoundVolume << std::endl;
	File << "music_volume=" << MusicVolume << std::endl;
	File << "locale=" << Locale << std::endl;

	// Write out input map
	ae::Actions.Serialize(File, ae::_Input::KEYBOARD);
	ae::Actions.Serialize(File, ae::_Input::MOUSE_AXIS);
	ae::Actions.Serialize(File, ae::_Input::MOUSE_BUTTON);

	File.close();
}
