/******************************************************************************
* Empty Clip
* Copyright (C) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <states/convert.h>
#include <ae/mesh.h>
#include <framework.h>
#include <map.h>

_ConvertState ConvertState;

void _ConvertState::Init() {
	Framework.Log << "Converting " << Param1 << std::endl;

	switch(Mode) {
		case 1: {
			_Map *Map = new _Map(Param1);
			Map->Save(Param1);
		} break;
		case 2: {
			ae::_Mesh::ConvertOBJ(Param1, true, true);
		} break;
	}

	Framework.Done = true;
}
