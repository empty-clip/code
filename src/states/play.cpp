/******************************************************************************
* Empty Clip
* Copyright (C) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#include <states/play.h>
#include <ae/actions.h>
#include <ae/assets.h>
#include <ae/audio.h>
#include <ae/camera.h>
#include <ae/console.h>
#include <ae/font.h>
#include <ae/framebuffer.h>
#include <ae/graphics.h>
#include <ae/light.h>
#include <ae/program.h>
#include <ae/random.h>
#include <ae/util.h>
#include <objects/monster.h>
#include <objects/particle.h>
#include <objects/player.h>
#include <states/editor.h>
#include <states/null.h>
#include <actiontype.h>
#include <config.h>
#include <events.h>
#include <framework.h>
#include <gameassets.h>
#include <hud.h>
#include <map.h>
#include <menu.h>
#include <objectmanager.h>
#include <particles.h>
#include <save.h>
#include <stats.h>
#include <SDL_keycode.h>
#include <glm/gtx/rotate_vector.hpp>
#include <algorithm>

_PlayState PlayState;

// Load level and set up objects
void _PlayState::Init() {
	ae::Graphics.SetViewport(ae::Graphics.CurrentSize);
	ae::Graphics.Element->SetActive(false);
	ae::Graphics.Element->Active = true;

	// Check for player
	if(TestMode) {
		Player = new _Player(Stats.Objects.at("player"));
		if(Framework.BenchMode) {
			Player->Name = "test";
			Player->RecalculateStats();
			Player->Health = Player->MaxHealth;
		}
		else {
			Player->SavePath = Config.SavePath + "test.save";
			try {
				Save.LoadPlayer(Player);
			}
			catch(std::invalid_argument &Error) {

				// Create new test save
				Player->Name = "test";
				Player->RecalculateStats();
				Player->Health = Player->MaxHealth;
				Player->TestSave = true;
				Save.SavePlayer(Player);
			}
		}

		Player->TestSave = true;
	}

	// Bad player
	if(!Player)
		throw std::runtime_error("Player is nullptr");

	// Set up labels
	SetFilterTextValue(ae::Assets.Elements["label_hud_filters_gear"], Player->GetFilterValue(FILTER_GEAR));
	SetFilterTextValue(ae::Assets.Elements["label_hud_filters_mods"], Player->GetFilterValue(FILTER_MODS));

	// Set checkpoint from editor
	if(FromEditor)
		Player->CheckpointIndex = CheckpointIndex;

	// Check for level override
	if(Level.empty())
		Level = Stats.FirstLevel;

	// Create framebuffer for mixing lights
	Framebuffer = new ae::_Framebuffer(ae::Graphics.CurrentSize);

	// Load level
	Map = new _Map(Level, Player->Clock, (size_t)Player->Progression);
	Map->InitializeTiles();
	Player->Map = Map;
	Player->MapID = Map->Filename;

	// Set starting position
	if(TestMode && SpawnLocation.x >= 0.0f)
		Player->WarpPosition(SpawnLocation);
	else
		Player->WarpPosition(Map->GetStartingPositionByCheckpoint(Player->CheckpointIndex));

	// Spawn objects
	Monsters.reserve((size_t)Map->Monsters);
	for(const auto &ObjectSpawn : Map->ObjectSpawns)
		SpawnObject(ObjectSpawn, false, Map->GetAddedLevel(), 0);

	// Initialize camera
	ae::_CameraSettings CameraSettings;
	CameraSettings.UpdateDivisor = CAMERA_DIVISOR;
	CameraSettings.SnappingThreshold = CAMERA_SNAPPING_THRESHOLD;
	Camera = new ae::_Camera(CameraSettings);
	Camera->CalculateFrustum(ae::Graphics.AspectRatio);
	Camera->ForcePosition(glm::vec3(Player->Position, CAMERA_DISTANCE));
	Map->Camera = Camera;

	// Initialize particles
	Particles = new _Particles();
	Particles->Camera = Camera;
	Particles->Map = Map;

	// Initialize HUD
	HUD = new _HUD(Camera, Player);
	HUD->SetStats(Map->Monsters, Map->Crates, Map->Secrets);
	if(Player->CheckpointIndex == 0 && !Map->Name.empty())
		HUD->ShowLevelName(Map->Name, UI_LEVELNAME_TIME);

	Camera->ConvertScreenToWorld(ae::Input.GetMouse(), WorldCursor);
	PreviousWorldCursor = WorldCursor;
	Menu.ShowDefaultCursor(false);

	ae::Actions.ResetState();
	ae::Audio.Stop();

	// Print stats
	if(DevMode)
		Framework.Console->AddMessage("TotalExperience=" + std::to_string(Map->TotalExperience));

	ActiveAI = 0;
	Timer = 0.0;
	SaveTimer = 0.0;
	PreviousTouchingEndEvent = nullptr;
	TouchingEndEvent = nullptr;
	LockedSound = nullptr;
	CursorItem = nullptr;
	PreviousCursorItem = nullptr;
}

// Close map
void _PlayState::Close() {
	if(Framework.BenchMode)
		Framework.Log << "ElapsedTime=" << Framework.ElapsedTime << " Frames=" << Framework.Frames << " PlayerPosition=" << Player->Position.x << "," << Player->Position.y << std::endl;

	// Player has already been penalized
	if(!Player->Hardcore && Player->IsDead()) {
		Player->Health = 1;
		Player->CombatTimer = GAME_COMBAT_TIMER;
	}

	Save.SavePlayer(Player);

	DeleteMonsters();
	ActiveEvents.clear();

	Player->StopAudio();

	if(TestMode) {
		delete Player;
		Player = nullptr;
	}

	delete Particles;
	delete Camera;
	delete Map;
	delete HUD;
	delete Framebuffer;

	Particles = nullptr;
	Camera = nullptr;
	Map = nullptr;
	HUD = nullptr;
	Camera = nullptr;
	Framebuffer = nullptr;
}

// Action handler
bool _PlayState::HandleAction(int InputType, size_t Action, int Value, bool Repeat) {
	if(Value == 0)
		return false;

	// Handle console toggling
	if(Action == Action::MISC_CONSOLE) {
		ae::Actions.ResetState();
		Framework.Console->Toggle();
		Framework.IgnoreNextInputEvent = true;
	}

	// Ignore actions when console is open
	if(Framework.Console->IsOpen())
		return false;

	// Handle console toggling
	if(Action == Action::MISC_DEBUG) {
		DebugMode = !DebugMode;
		return false;
	}

	if(!Player || IsPaused())
		return false;

	if(!Player->IsDying()) {
		switch(Action) {
			case Action::GAME_INVENTORY:
				HUD->SetInventoryOpen(!HUD->InventoryOpen);
				Player->SetAiming(false);
				Player->SetSprinting(false);
			break;
			case Action::GAME_SORTINVENTORY:
				if(HUD->InventoryOpen && !HUD->CursorItem)
					Player->SortInventory();
			break;
			case Action::GAME_FIRE:
				if(!HUD->InventoryOpen && !Player->IsMeleeAttacking()) {

					// Use melee weapon if player has no main hand
					int AttackType = WEAPONATTACK_MAIN;
					if(!Player->GetMainHand())
						AttackType = WEAPONATTACK_MELEE;

					// Cancel reload
					if(Player->Reloading && Player->WeaponHasAmmo(AttackType))
						Player->CancelReloading();

					// Check ammo
					if(Player->CanAttack(AttackType) && !Player->WeaponHasAmmo(AttackType)) {
						if(Player->CanReload())
							Player->StartReloading();
						else
							ae::Audio.PlaySound(Player->GetSound(SOUND_EMPTY, AttackType));
					}

					if(Player->CheckAttackTimer(AttackType) && Player->FireRateType[AttackType] == FIRERATE_SEMI && (!Player->BurstRounds[AttackType] || (Player->BurstRounds[AttackType] && Player->BurstRoundsShot == 0))) {
						Player->BurstRoundsShot = 0;
						Player->RequestAttack(AttackType);
					}
				}
			break;
			case Action::GAME_MELEE:
				if(!HUD->InventoryOpen) {
					if(Player->Reloading)
						Player->CancelReloading();

					if(Player->FireRateType[WEAPONATTACK_MELEE] == FIRERATE_SEMI)
						Player->RequestAttack(WEAPONATTACK_MELEE);
				}
			break;
			case Action::GAME_RELOAD:
				if(!HUD->CursorItem)
					Player->StartReloading();
			break;
			case Action::GAME_WEAPONSWITCH:
				if(!HUD->CursorItem)
					Player->StartWeaponSwitch(_Slot(&Player->GetActiveOutfitBag(), GearType::MAINHAND), _Slot(&Player->GetActiveOutfitBag(), GearType::OFFHAND));
			break;
			case Action::GAME_SWITCHOUTFIT:
				Player->StartOutfitSwitch(!Player->ActiveOutfit);
			break;
			case Action::GAME_FLASHLIGHT:
				Player->Flashlight = !Player->Flashlight;
				ae::Audio.PlaySound(ae::Assets.Sounds["game_flashlight0.ogg"]);
			break;
			case Action::GAME_USE:
				if(TouchingEndEvent)
					EndLevel();
			break;
			case Action::GAME_FILTERGEAR:
				ChangeFilterLevel(FILTER_GEAR);
			break;
			case Action::GAME_FILTERMODS:
				ChangeFilterLevel(FILTER_MODS);
			break;
			case Action::GAME_MINIMAP:
				Player->MinimapSizeIndex += ae::Input.ModKeyDown(KMOD_SHIFT) ? -1 : 1;
				if(Player->MinimapSizeIndex >= (int)Map->MinimapSizes.size())
					Player->MinimapSizeIndex = 0;
				else if(Player->MinimapSizeIndex < 0)
					Player->MinimapSizeIndex = (int)Map->MinimapSizes.size() - 1;
			break;
			case Action::MISC_MENU:
				HUD->SetInventoryOpen(false);
				Save.SavePlayer(Player);
				Menu.InitInGame();
			break;
		}
	}
	else {
		if(Action == Action::GAME_USE)
			Player->Respawn();
	}

	return false;
}

// Key handler
bool _PlayState::HandleKey(const ae::_KeyEvent &KeyEvent) {
	if(Framework.BenchMode) {
		if(KeyEvent.Pressed && KeyEvent.Scancode == SDL_SCANCODE_ESCAPE)
			Framework.Done = true;

		return false;
	}

	bool Handled = ae::Graphics.Element->HandleKey(KeyEvent);

	bool SendAction = true;
	if(IsPaused()) {
		Player->StopAudio();
		if(!Handled)
			SendAction = Menu.HandleKey(KeyEvent);

		return SendAction;
	}

	if(KeyEvent.Pressed) {
		switch(KeyEvent.Scancode) {
			case SDL_SCANCODE_ESCAPE:
				if(Player->IsDead()) {
					if(Player->Hardcore) {
						Framework.ChangeState(&NullState);
					}
					else {
						Player->Respawn();
						Save.SavePlayer(Player);
					}
				}
				else if(!Player->IsDying()) {
					if(HUD->InventoryOpen) {
						HUD->SetInventoryOpen(false);
					}
					else if(TestMode) {
						if(FromEditor)
							Framework.ChangeState(&EditorState);
						else
							Framework.Done = true;
					}
					else {
						Save.SavePlayer(Player);
						Menu.InitInGame();
					}
				}
			break;
		}
	}

	return SendAction;
}

// Mouse handler
void _PlayState::HandleMouseButton(const ae::_MouseEvent &MouseEvent) {
	if(Framework.BenchMode)
		return;

	HUD->HandleMouseButton(MouseEvent);

	if(IsPaused())
		Menu.HandleMouseButton(MouseEvent);
}

// Handle console commands
bool _PlayState::HandleCommand(ae::_Console *Console) {

	// Get parameters
	std::vector<std::string> Parameters;
	ae::TokenizeString(Console->Parameters, Parameters);

	// Handle normal commands
	if(Console->Command == "quit") {
		HandleQuit();
		return true;
	}
	else if(Console->Command == "suicide") {
		if(Player) {
			Player->UpdateHealth(-10000000);
			PlayerDied();
		}

		return true;
	}

	// Handle dev commands
	if(DevMode) {
		if(Console->Command == "ammo") {
			if(!Player)
				return true;

			for(const auto &Ammo : Stats.AmmoNames)
				Player->Ammo[Ammo] = Player->AmmoMax[Ammo];

			Player->UpdateAmmoNeeded();
			Console->AddMessage("ammo replenished");
		}
		else if(Console->Command == "clock") {
			if(!Player)
				return true;

			if(Parameters.size() == 1)
				Player->Clock = std::clamp(ae::ToNumber<double>(Parameters[0]), 0.0, MAP_DAY_LENGTH);
			else {
				Console->AddMessage("usage: " + Console->Command + " [value]");
				Console->AddMessage("clock = " + std::to_string(Player->Clock));
			}
		}
		else if(Console->Command == "experience") {
			if(!Player)
				return true;

			if(Parameters.size() == 1) {
				bool Adjust = false;
				if(Parameters[0][0] == '+' || Parameters[0][0] == '-')
					Adjust = true;

				int64_t Change = ae::ToNumber<int64_t>(Parameters[0]);
				Player->Experience = std::max((int64_t)0, Adjust ? Player->Experience + Change : Change);
				Player->RecalculateStats();
			}
			else
				Console->AddMessage("usage: " + Console->Command + " [+-][amount]");
		}
		else if(Console->Command == "god") {
			if(!Player)
				return true;

			GodMode = !GodMode;
			Console->AddMessage("god = " + std::to_string(GodMode));
		}
		else if(Console->Command == "health") {
			if(!Player)
				return true;

			if(Parameters.size() == 1) {
				bool Adjust = false;
				if(Parameters[0][0] == '+' || Parameters[0][0] == '-')
					Adjust = true;

				int Change = ae::ToNumber<int>(Parameters[0]);

				if(Adjust)
					Player->UpdateHealth(Change);
				else
					Player->Health = std::max(1, Change);

				Player->RecalculateStats();
			}
			else
				Console->AddMessage("usage: " + Console->Command + " [+-][amount]");
		}
		else if(Console->Command == "keys") {
			if(!Player)
				return true;

			Player->Keys["key_red"] = 1;
			Player->Keys["key_green"] = 1;
			Player->Keys["key_blue"] = 1;
			Player->Keys["key_boss"] = 1;

			Console->AddMessage("keys acquired");
		}
		else if(Console->Command == "progression") {
			if(!Player)
				return true;

			if(Parameters.size() == 1) {
				Player->Progression = std::clamp(ae::ToNumber<int>(Parameters[0]), 1, GAME_MAX_PROGRESSION);
				Player->ProgressionTime = 0;
				Player->ProgressionKills = 0;
				Player->ProgressionCrates = 0;
				Player->ProgressionSecrets = 0;
				Player->ProgressionDeaths = 0;
				Player->RecalculateStats();
				Player->BuildFilterValues();
			}
			else {
				Console->AddMessage("usage: " + Console->Command + " [value]");
				Console->AddMessage("progression = " + std::to_string(Player->Progression));
			}
		}
		else if(Console->Command == "quality") {
			if(Parameters.size() == 1)
				DefaultQuality = ae::ToNumber<int>(Parameters[0]);
			else {
				Console->AddMessage("usage: " + Console->Command + " [value]");
				Console->AddMessage("quality = " + std::to_string(DefaultQuality));
			}

			return true;
		}
		else if(Console->Command == "reset") {
			if(!Player)
				return true;

			if(Parameters.size() == 1) {
				if(Parameters[0] == "skills") {
					for(int i = 0; i < SKILL_COUNT; i++)
						Player->Skills[i] = 0;

					Player->RecalculateStats();
					Console->AddMessage("skills reset");
				}
				else if(Parameters[0] == "keys") {
					Player->Keys.clear();
					Console->AddMessage("keys reset");
				}
			}
			else {
				Console->AddMessage("usage: reset [skills|keys]");
			}
		}
		else
			return false;
	}
	else
		return false;

	return true;
}

// Window size updates
void _PlayState::HandleWindow(uint8_t Event) {
	if(Event != SDL_WINDOWEVENT_SIZE_CHANGED)
		return;

	if(Camera)
		Camera->CalculateFrustum(ae::Graphics.AspectRatio);

	Menu.HandleResize();

	if(Framebuffer) {
		Framebuffer->Resize(ae::Graphics.CurrentSize);
		ae::Graphics.ResetState();
	}
}

// Handle quit events
void _PlayState::HandleQuit() {
	Framework.Done = true;
}

// Update
void _PlayState::Update(double FrameTime) {
	ae::Graphics.Element->Update(FrameTime, ae::Input.GetMouse());
	//if(ae::Graphics.Element->HitElement)
	//	std::cout << ae::Graphics.Element->HitElement->ID << std::endl;

	int OldSkillPointsRemaining = Player->SkillPointsRemaining;
	int OldLevel = Player->Level;
	ActiveProjectiles = 0;
	Timer += FrameTime;
	SaveTimer += FrameTime;
	FlashTimer = std::max(0.0, FlashTimer - FrameTime);
	Menu.Update(FrameTime);

	// Handle pause
	if(IsPaused() || Player->IsDead()) {
		Menu.ShowDefaultCursor(true);
		HUD->HoverItem = nullptr;
		HUD->CursorUseWorldPosition = false;

		return;
	}

	// Get world cursor
	PreviousWorldCursor = WorldCursor;
	Camera->ConvertScreenToWorld(ae::Input.GetMouse(), WorldCursor);

	// Handle input
	if(!Player->IsDying() && ae::FocusedElement == nullptr && !Framework.BenchMode) {

		// Turn character to face the world cursor
		if(Player->Action != ACTION_MELEE)
			Player->FacePosition(WorldCursor);

		// Move types
		if(ae::Actions.State[Action::GAME_UP].Value > 0.0f && ae::Actions.State[Action::GAME_LEFT].Value > 0.0f)
			Player->MoveState = MOVE_FORWARDLEFT;
		else if(ae::Actions.State[Action::GAME_UP].Value > 0.0f && ae::Actions.State[Action::GAME_RIGHT].Value > 0.0f)
			Player->MoveState = MOVE_FORWARDRIGHT;
		else if(ae::Actions.State[Action::GAME_DOWN].Value > 0.0f && ae::Actions.State[Action::GAME_LEFT].Value > 0.0f)
			Player->MoveState = MOVE_BACKWARDLEFT;
		else if(ae::Actions.State[Action::GAME_DOWN].Value > 0.0f && ae::Actions.State[Action::GAME_RIGHT].Value > 0.0f)
			Player->MoveState = MOVE_BACKWARDRIGHT;
		else if(ae::Actions.State[Action::GAME_LEFT].Value > 0.0f)
			Player->MoveState = MOVE_LEFT;
		else if(ae::Actions.State[Action::GAME_RIGHT].Value > 0.0f)
			Player->MoveState = MOVE_RIGHT;
		else if(ae::Actions.State[Action::GAME_UP].Value > 0.0f)
			Player->MoveState = MOVE_FORWARD;
		else if(ae::Actions.State[Action::GAME_DOWN].Value > 0.0f)
			Player->MoveState = MOVE_BACKWARD;
		else
			Player->MoveState = MOVE_NONE;

		// Attack or aim
		if(!HUD->InventoryOpen) {

			// Use melee weapon if player has no main hand
			int AttackType = WEAPONATTACK_MAIN;
			if(!Player->GetMainHand())
				AttackType = WEAPONATTACK_MELEE;

			// Check holding down fire button to attack
			if(!Player->IsMeleeAttacking() && Player->FireRateType[AttackType] == FIRERATE_AUTO && ae::Actions.State[Action::GAME_FIRE].Value > 0.0f)
				Player->RequestAttack(AttackType);

			// Check holding down melee button to attack
			else if(!Player->IsMeleeAttacking() && Player->FireRateType[WEAPONATTACK_MELEE] == FIRERATE_AUTO && ae::Actions.State[Action::GAME_MELEE].Value > 0.0f)
				Player->RequestAttack(WEAPONATTACK_MELEE);

			// Aim
			Player->SetAiming(ae::Actions.State[Action::GAME_AIM].Value > 0.0f && !Player->Reloading && !Player->IsSwitching());
			Player->SetSprinting(ae::Actions.State[Action::GAME_SPRINT].Value > 0.0f);

			// Don't show tooltips while firing
			if(Player->AttackRequested)
				CursorItemTimer = 0.0;
		}

		Player->UseRequested = ae::Actions.State[Action::GAME_USE].Value;
	}
	else
		HUD->SetInventoryOpen(false);

	// Update minimap
	SetMinimapCaptureSize();

	// Walk right during bench mode
	if(Framework.BenchMode) {
		Player->FacePosition(glm::vec2(1000.0f, 0.0f));
		Player->MoveState = MOVE_RIGHT;
	}

	int64_t PlayerHealth = Player->Health;

	// Update player
	Player->Update(FrameTime);
	if(Player->PositionChanged)
		IgnoreItems.clear();

	if(GodMode)
		Player->Stamina = Player->MaxStamina;

	// Handle gun flashes
	if(Player->Action == ACTION_STARTSHOOT && Player->GetMainHand() && Player->GetMainHand()->Template.Attributes.at("flash").Int)
		FlashTimer = LIGHT_FLASH_TIME;

	// Check for events
	if(Player->TileChanged)
		CheckEvents(Player);

	// Activate events
	if(Player->ApplyUse())
		ActivateEvent();

	// Handle auto and manually picking up items
	HandlePickup();
	Player->UseRequested = false;

	// Update objects
	Map->Update(FrameTime, Player->Clock);
	Map->ObjectManager->RenderList[_ObjectManager::RENDER_PLAYER].push_back(Player);

	// Update monsters
	UpdateMonsters(FrameTime);

	// Check for player dying
	if(Player->Health == 0 && PlayerHealth > 0)
		PlayerDied();

	// Update particles
	Particles->Update(FrameTime);

	// Add player to minimap
	_MinimapIcon MinimapIcon;
	MinimapIcon.Bounds = glm::vec4(
		Player->Position.x - Player->Scale * 0.25f, Player->Position.y - Player->Scale * 0.25f,
		Player->Position.x + Player->Scale * 0.25f, Player->Position.y + Player->Scale * 0.25f
	);
	Map->MinimapIcons[_Map::MINIMAP_PLAYER].push_back(MinimapIcon);

	// Update events
	UpdateEvents(FrameTime);

	// Apply the damage
	if(Player->AttackMade)
		ResolveAttack(Player, GRID_MONSTER);

	// Level up message
	if(Player->Level > OldLevel) {
		std::ostringstream Buffer;
		Buffer << "LEVEL UP!";
		if(Player->SkillPointsRemaining > OldSkillPointsRemaining)
			Buffer << " YOU HAVE UNSPENT SKILL POINTS!";

		HUD->ShowTextMessage(Buffer.str(), 5.0);
		Save.SavePlayer(Player);
	}

	// Show end message
	if(TouchingEndEvent)
		HUD->ShowMessageBox("Press [c red]" + ae::Actions.GetInputNameForAction(Action::GAME_USE) + "[c white] to end the level", 9999999.0, UI_MESSAGE_SMALL_SIZE);

	// Update camera
	Camera->Set2DPosition(Player->Position);

	// Get zoom state
	if(Player->Aiming) {
		glm::vec2 CursorVector = WorldCursor - Player->Position;
		if(CursorVector.x != 0.0f && CursorVector.y != 0.0f) {
			Map->CollisionHits.clear();
			Map->CheckBulletCollisions(Player, glm::normalize(CursorVector), Map->CollisionHits, GRID_MONSTER, true, 1, _Tile::VISION);
			if(Map->CollisionHits.size()) {
				glm::vec2 HitVector = Map->CollisionHits.front().Position - Player->Position;
				if(glm::dot(CursorVector, CursorVector) < glm::dot(HitVector, HitVector))
					Camera->UpdatePosition(CursorVector / Player->ZoomScale);
				else
					Camera->UpdatePosition(HitVector / Player->ZoomScale);
			}
		}

		Camera->SetDistance(CAMERA_DISTANCE_AIMED);
	}
	else
		Camera->SetDistance(CAMERA_DISTANCE);

	Camera->Update(FrameTime);

	// Get item at cursor
	PreviousCursorItem = CursorItem;
	CursorItem = Map->GetClosestItem(WorldCursor, ShowMoreInfo());
	if(CursorItem && CursorItem == PreviousCursorItem)
		CursorItemTimer += FrameTime;
	else
		CursorItemTimer = 0.0;

	// Update the HUD
	HUD->Update(FrameTime, Player->GetCrosshairRadius(WorldCursor), Player->Clock);

	// Reset timer if player isn't idle
	if(Player->Action != ACTION_IDLE)
		ClosestItemTimer = 0.0;

	// Show item tooltip when standing over item
	if(!HUD->InventoryOpen && !Player->Aiming && ClosestItem && ClosestItem == LastClosestItem && !ClosestItem->CanAutoPickup() && !ClosestItem->Filtered) {
		ClosestItemTimer += FrameTime;
		if(!HUD->HoverItem && ClosestItemTimer >= HUD_STANDOVER_TIME) {
			HUD->HoverItem = ClosestItem;
			HUD->CursorUseWorldPosition = true;
		}
	}
	else
		ClosestItemTimer = 0.0;

	LastClosestItem = ClosestItem;

	// Set cursor item
	if(SetHoverItem()) {
		HUD->HoverItem = CursorItem;
		HUD->CursorUseWorldPosition = false;
	}

	// Update audio
	ae::Audio.SetPosition(glm::vec3(Player->Position.x, AUDIO_POSITION_Z, Player->Position.y));

	// Autosave
	if(SaveTimer >= SAVE_TIME) {
		SaveTimer = 0.0;
		Save.SavePlayer(Player);
	}
}

// Render the state
void _PlayState::Render(double BlendFactor) {
	if(IsPaused() || Player->IsDead())
		BlendFactor = 0;

	// Set up programs
	ae::_Program *MapProgram = ae::Assets.Programs["map"];
	ae::_Program *MapNormProgram = ae::Assets.Programs["map_norm"];
	ae::_Program *ItemProgram = ae::Assets.Programs["item"];

	// Get player light
	glm::vec4 PlayerLightColor;
	glm::vec3 LightAttenuantion;
	if(Config.WeaponFlashes && FlashTimer > 0.0) {
		PlayerLightColor = LIGHT_FLASH_COLOR;
		LightAttenuantion = LIGHT_FLASH_ATTENUATION;
	}
	else {
		PlayerLightColor = PLAYER_LIGHT;
		LightAttenuantion = LIGHT_ATTENUATION;
	}

	// Set up lights
	glm::vec3 LightPosition(glm::vec2(Player->Position), 1.0f);
	MapProgram->LightCount = 1;
	MapProgram->Lights[0].Color = PlayerLightColor;
	MapProgram->Lights[0].Position = LightPosition;
	MapProgram->Lights[0].Attenuation = LightAttenuantion;
	MapProgram->AmbientLight = Map->AmbientLight;
	MapProgram->FogColor = Map->FogColor;
	MapNormProgram->LightCount = 1;
	MapNormProgram->Lights[0].Color = PlayerLightColor;
	MapNormProgram->Lights[0].Position = LightPosition;
	MapNormProgram->Lights[0].Attenuation = LightAttenuantion;
	MapNormProgram->AmbientLight = Map->AmbientLight;
	MapNormProgram->FogColor = Map->FogColor;
	ItemProgram->LightCount = 1;
	ItemProgram->Lights[0].Color = PlayerLightColor;
	ItemProgram->Lights[0].Position = LightPosition;
	ItemProgram->Lights[0].Attenuation = LightAttenuantion;
	ItemProgram->AmbientLight = Map->AmbientLight;
	ItemProgram->FogColor = Map->FogColor;

	// Setup the viewing matrix
	ae::Graphics.Setup3D();
	Camera->Set3DProjection(BlendFactor);

	// Setup the viewing matrix
	ae::Graphics.SetProgram(MapProgram);
	MapProgram->SetUniformMat4("view_projection_transform", Camera->Transform);
	ae::Graphics.SetProgram(MapNormProgram);
	MapNormProgram->SetUniformMat4("view_projection_transform", Camera->Transform);
	ae::Graphics.SetProgram(ItemProgram);
	ItemProgram->SetUniformMat4("view_projection_transform", Camera->Transform);
	ae::Graphics.SetProgram(ae::Assets.Programs["pos"]);
	ae::Assets.Programs["pos"]->SetUniformMat4("view_projection_transform", Camera->Transform);
	ae::Graphics.SetProgram(ae::Assets.Programs["pos_uv"]);
	ae::Assets.Programs["pos_uv"]->SetUniformMat4("view_projection_transform", Camera->Transform);
	ae::Graphics.SetProgram(ae::Assets.Programs["text"]);
	ae::Assets.Programs["text"]->SetUniformMat4("view_projection_transform", Camera->Transform);
	ae::Graphics.SetProgram(ae::Assets.Programs["minimap"]);
	ae::Assets.Programs["minimap"]->SetUniformMat4("view_projection_transform", ae::Graphics.Ortho);

	// Update minimap
	SetMinimapCaptureSize();

	// Add lights
	Framebuffer->Clear();
	ae::Graphics.SetProgram(ae::Assets.Programs["pos_uv"]);
	ae::Graphics.EnableParticleBlending();
	if(Player->Flashlight) {
		glm::vec2 DrawPosition;
		Player->GetDrawPosition(DrawPosition, BlendFactor);
		ae::Graphics.SetColor(glm::vec4(1.0f));
		ae::Graphics.DrawSprite(glm::vec3(DrawPosition + PLAYER_FLASHLIGHT_SIZE.x * Player->GetDirectionVector(), 0), ae::Assets.Textures["textures/lights/flashlight.png"], Player->Rotation, PLAYER_FLASHLIGHT_SIZE);
	}
	Map->ObjectManager->RenderLights(_ObjectManager::RENDER_LIGHTS, BlendFactor);
	ae::Graphics.DisableParticleBlending();
	ae::_Framebuffer::Unbind();

	// Set framebuffer texture
	if(Framebuffer) {
		MapProgram->Use();
		ae::Graphics.SetActiveTexture(1);
		Framebuffer->BindTexture();
		ae::Graphics.SetActiveTexture(0);
		MapNormProgram->Use();
		ae::Graphics.SetActiveTexture(1);
		Framebuffer->BindTexture();
		ae::Graphics.SetActiveTexture(0);
		ItemProgram->Use();
		ae::Graphics.SetActiveTexture(1);
		Framebuffer->BindTexture();
		ae::Graphics.SetActiveTexture(0);
	}

	int BlockRenderCount = 0;
	int ParticleRenderCount = 0;
	int PropRenderCount = 0;
	int ItemRenderCount = 0;

	// Draw the floor
	BlockRenderCount += Map->RenderFloors();

	// Draw floor decals
	ae::Graphics.SetProgram(MapProgram);
	MapProgram->ResetTransform(MapProgram->TextureTransformID);
	MapProgram->ResetTransform(MapProgram->NormalTransformID);
	ae::Graphics.SetDepthMask(false);
	ae::Graphics.SetDepthTest(false);
	if(Config.FloorDecals)
		ParticleRenderCount += Map->RenderParticles(_Particles::FLOOR_DECALS, BlendFactor);

	// Draw walls and props below objects
	BlockRenderCount += Map->RenderWalls(true);
	PropRenderCount += Map->RenderProps();

	// Draw items
	ae::Graphics.SetProgram(ItemProgram);
	ae::Graphics.SetDepthMask(false);
	ae::Graphics.SetDepthTest(true);
	ItemRenderCount += Map->ObjectManager->RenderItems(BlendFactor);
	ae::Graphics.ResetState();

	// Draw objects
	ae::Graphics.SetProgram(MapProgram);
	MapProgram->ResetTransform(MapProgram->TextureTransformID);
	ItemRenderCount += Map->ObjectManager->Render(_ObjectManager::RENDER_ITEMS, BlendFactor);
	Map->ObjectManager->Render(_ObjectManager::RENDER_PROJECTILES, BlendFactor);
	Map->ObjectManager->Render(_ObjectManager::RENDER_PLAYER, BlendFactor);
	Map->ObjectManager->Render(_ObjectManager::RENDER_MONSTER, BlendFactor);

	// Draw walls and props again on top of objects
	BlockRenderCount += Map->RenderWalls();
	BlockRenderCount += Map->RenderFlatWalls();
	PropRenderCount += Map->RenderProps();

	// Draw wall decals
	ae::Graphics.SetProgram(MapNormProgram);
	MapProgram->ResetTransform(MapNormProgram->TextureTransformID);
	MapProgram->ResetTransform(MapNormProgram->NormalTransformID);
	ae::Graphics.SetDepthMask(false);
	if(Config.WallDecals)
		ParticleRenderCount += Map->RenderParticles(_Particles::WALL_DECALS, BlendFactor);

	// Draw particles
	ae::Graphics.EnableParticleBlending();
	ae::Graphics.SetProgram(MapProgram);
	MapProgram->ResetTransform(MapProgram->TextureTransformID);
	MapProgram->ResetTransform(MapProgram->NormalTransformID);
	Particles->Render(_Particles::NORMAL, BlendFactor);
	ae::Graphics.DisableParticleBlending();

	// Emissive animating particles
	ae::Graphics.SetProgram(ae::Assets.Programs["pos_uv"]);
	Particles->Render(_Particles::EMISSIVE_ANIMATION, BlendFactor);

	// Normal emissive particles
	ae::Graphics.EnableParticleBlending();
	ae::Assets.Programs["pos_uv"]->ResetTransform(ae::Assets.Programs["pos_uv"]->TextureTransformID);
	Particles->Render(_Particles::EMISSIVE, BlendFactor);
	ae::Graphics.DisableParticleBlending();

	// Draw the foreground tiles
	BlockRenderCount += Map->RenderForeground(Player->Position, false);

	// Get cursor position
	glm::vec2 CursorDrawPosition = (WorldCursor == PreviousWorldCursor) ? WorldCursor : WorldCursor * (float)BlendFactor + PreviousWorldCursor * (float)(1.0f - BlendFactor);
	if(!Player->IsDying())
		HUD->DrawCrosshair(CursorDrawPosition);

	// Debug
	if(GodMode && DevMode && DebugMode) {
		ae::Graphics.SetDepthTest(false);

		// Draw monster target positions
		ae::Graphics.SetProgram(ae::Assets.Programs["pos"]);
		ae::Graphics.SetColor(COLOR_RED);
		for(const auto &Object : Map->ObjectManager->RenderList[_ObjectManager::RENDER_MONSTER]) {
			_Entity *Entity = (_Entity *)Object;
			ae::Graphics.DrawCircle(glm::vec3(Entity->TargetPosition, 0), Entity->TargetRadius);
		}

		// Draw weapon ranges
		glm::vec2 DrawPosition;
		Player->GetDrawPosition(DrawPosition, BlendFactor);
		for(int i = 0; i < WEAPONATTACK_COUNT; i++) {
			glm::vec4 Color = COLOR_WHITE;
			if(i == 1)
				Color = COLOR_GREEN;

			float Range = Player->AttackRange[i];
			if(Range == 0.0f)
				Range = 100.0f;

			ae::Graphics.SetProgram(ae::Assets.Programs["pos"]);
			ae::Graphics.SetColor(Color);
			ae::Graphics.DrawCircle(glm::vec3(DrawPosition, 0), Range);

			glm::vec2 Direction = Player->GetDirectionVector();
			glm::vec2 NormalDirection(-Direction.y, Direction.x);

			glm::vec2 LeftLineStart = DrawPosition - NormalDirection * (Player->AttackWidth[i] - Player->MeleeOffset[i]);
			glm::vec2 LeftLineEnd = LeftLineStart + Direction * Range;
			ae::Graphics.DrawLine(LeftLineStart, LeftLineEnd);

			glm::vec2 RightLineStart = DrawPosition + NormalDirection * (Player->AttackWidth[i] + Player->MeleeOffset[i]);
			glm::vec2 RightLineEnd = RightLineStart + Direction * Range;
			ae::Graphics.DrawLine(RightLineStart, RightLineEnd);
			//glm::vec2 LeftLine = Player->Position + Player->GetDirectionVector(-Player->MaxAccuracy[i] * 0.5f) * Range;
			//glm::vec2 RightLine = Player->Position + Player->GetDirectionVector(Player->MaxAccuracy[i] * 0.5f) * Range;
			//ae::Graphics.DrawLine(Player->Position, LeftLine);
			//ae::Graphics.DrawLine(Player->Position, RightLine);
		}

		//ae::Graphics.SetProgram(ae::Assets.Programs["pos"]);
		//ae::Graphics.SetColor(COLOR_BLUE);
		//ae::Graphics.DrawRectangle3D(glm::vec2(Player->LastGoodCoord) + glm::vec2(0.5-0.25), glm::vec2(Player->LastGoodCoord) + glm::vec2(0.5+0.25), true);

		ae::Graphics.SetDepthTest(true);
	}

	// Setup OpenGL for drawing the HUD
	ae::Graphics.Setup2D();
	ae::Graphics.SetStaticUniforms();
	ae::Graphics.SetDepthTest(false);
	ae::Graphics.SetDepthMask(false);

	// Draw more info
	if(ShowMoreInfo()) {

		// Draw item level/quality
		glm::vec2 TextPosition;
		std::ostringstream Buffer;
		glm::vec4 Color;
		for(auto Iterator : Map->ObjectManager->RenderList[_ObjectManager::RENDER_ITEMS]) {
			_Item *Item = (_Item *)Iterator;
			if(!Item->CanShowMoreInfo())
				continue;

			// Skip item being dragged
			if(Item == HUD->CursorItem)
				continue;

			// Skip filtered items
			if(Item->Filtered)
				continue;

			Camera->ConvertWorldToScreen(Iterator->Position, TextPosition, BlendFactor);

			// Show bonus value
			switch(Item->Type) {
				case _Object::MOD: {
					if(Item->IsChangeMod()) {
						Buffer << "+" << ae::Round2(Item->Attributes.at("bonus_2nd").Float) << "%";
						ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), glm::ivec2(TextPosition + glm::vec2(0, -14) * ae::_Element::GetUIScale()), ae::CENTER_BASELINE, COLOR_FAINT_WHITE);
						Buffer.str("");
					}
					else {
						if(!Item->Template.Attributes.at("negative").Int)
							Buffer << "+";
						Buffer << ae::Round2(Item->Attributes.at("bonus").Float);
						if(Item->Template.Attributes.at("percent_sign").Int)
							Buffer << "%";

						ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), glm::ivec2(TextPosition + glm::vec2(0, -14) * ae::_Element::GetUIScale()), ae::CENTER_BASELINE, COLOR_FAINT_WHITE);
						Buffer.str("");
					}
				} break;
				case _Object::USABLE: {
					switch(Item->Template.Attributes.at("usable_type").Int) {
						case USABLE_HAMMER:
							Buffer << Item->GetHammerQualityChange() << "%";
						break;
						case USABLE_WHETSTONE:
							Buffer << Item->GetWhetstoneQuality() << "%";
						break;
						case USABLE_WRENCH:
							Buffer << "+" << Item->GetWrenchLevel();
						break;
						case USABLE_DYNAMITE:
							Buffer << Item->GetDynamiteQuality() << "%";
						break;
						case USABLE_PLIERS:
							Buffer << Item->GetPliersLevel();
						break;
					}

					ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), glm::ivec2(TextPosition + glm::vec2(0, -14) * ae::_Element::GetUIScale()), ae::CENTER_BASELINE, COLOR_FAINT_WHITE);
					Buffer.str("");
				} break;
				default: {

					// Get colors
					Item->GetQualityColor(Color);
					Color.a = 0.7f;

					// Draw item level
					Buffer << Item->Level;
					ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), glm::ivec2(TextPosition + glm::vec2(0, -20) * ae::_Element::GetUIScale()), ae::CENTER_BASELINE, COLOR_FAINT_GOLD);
					Buffer.str("");

					// Draw item quality
					Buffer << Item->Quality << "%";
					ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), glm::ivec2(TextPosition + glm::vec2(0, 32) * ae::_Element::GetUIScale()), ae::CENTER_BASELINE, Color);
					Buffer.str("");
				} break;
			}
		}

		// Show accuracy in degrees
		if(!HUD->InventoryOpen) {
			Buffer << ae::Round1(Player->CurrentAccuracy);
			ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), ae::Input.GetMouse() + glm::ivec2(glm::vec2(7, 7) * ae::_Element::GetUIScale()), ae::LEFT_BASELINE);
			Buffer.str("");
		}
	}

	// Draw damage text numbers
	Particles->Render(_Particles::TEXT, BlendFactor);

	// Render HUD
	HUD->Render(ae::FocusedElement == nullptr && ae::Actions.State[Action::GAME_MAP].Value > 0.0f, BlendFactor);

	// Debug mode
	if(DebugMode || Framework.BenchMode) {
		glm::vec2 DrawPosition = glm::vec2(10, 230) * ae::_Element::GetUIScale();
		glm::vec2 Spacing(0, 16 * ae::_Element::GetUIScale());
		std::ostringstream Buffer;
		Buffer << ae::Graphics.FramesPerSecond << " FPS";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");

		DrawPosition.y += Spacing.y;
		Buffer << BlockRenderCount << " blocks rendered";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");

		DrawPosition.y += Spacing.y;
		Buffer << PropRenderCount << " props rendered";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");

		DrawPosition.y += Spacing.y;
		Buffer << ItemRenderCount << " items rendered";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");

		DrawPosition.y += Spacing.y;
		Buffer << Map->ObjectManager->RenderList[_ObjectManager::RENDER_MONSTER].size() << " monsters rendered";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");

		DrawPosition.y += Spacing.y;
		Buffer << ParticleRenderCount << " decals rendered";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");

		DrawPosition.y += Spacing.y;
		Buffer << ActiveProjectiles << " active projectiles";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");

		DrawPosition.y += Spacing.y;
		Buffer << ActiveAI << " active ai";
		ae::Assets.Fonts["hud_tiny"]->DrawText(Buffer.str(), DrawPosition);
		Buffer.str("");
	}

	// Show red overlay when player's health is low
	if(Player->GetHealthPercentage() <= HUD_PLAYER_HEALTH_WARNING) {
		ae::Graphics.SetProgram(ae::Assets.Programs["ortho_pos"]);
		float FadePower = (HUD_PLAYER_HEALTH_WARNING - Player->GetHealthPercentage()) / HUD_PLAYER_HEALTH_WARNING;
		float FadeAmount = HUD_PLAYER_HEALTH_FADE + sin(Timer * HUD_PLAYER_HEALTH_PULSE_FACTOR * FadePower) * HUD_PLAYER_HEALTH_PULSE_AMOUNT;
		ae::Graphics.SetColor(glm::vec4(1.0f, 0.0f, 0.0f, FadeAmount * FadePower));
		ae::Graphics.DrawRectangle(glm::vec2(0, 0), ae::Graphics.CurrentSize, true);
	}

	// Fade screen when paused
	if(IsPaused() || Player->IsDead())
		ae::Graphics.FadeScreen(ae::Assets.Programs["ortho_pos"], GAME_PAUSE_FADEAMOUNT);

	// Render menu
	Menu.Render();

	// Draw death screen
	if(Player->IsDead()) {
		Menu.ShowDefaultCursor(true);
		HUD->DrawDeathScreen();
	}
}

// Resolve an entity attacking
void _PlayState::ResolveAttack(_Entity *Attacker, int GridType) {
	Attacker->AttackMade = false;

	// Check for ammo
	if(!Attacker->WeaponHasAmmo(Attacker->AttackRequestType))
		return;

	// Add push
	Attacker->ApplyForce(-Attacker->Direction, Attacker->Push[Player->AttackRequestType], true);

	// Check achievements for weapons used
	if(Attacker->Type == _Object::PLAYER) {
		const char *WeaponID = Attacker->GetWeaponID(Attacker->AttackRequestType);
		if(WeaponID) {
			std::string WeaponIDString = std::string(WeaponID);
			if(Player->StatFistsOnly && WeaponIDString != "weapon_fists")
				Player->StatFistsOnly = false;
			if(Player->StatLoneWolf && WeaponIDString != "weapon_knife" && WeaponIDString != "weapon_boltrifle")
				Player->StatLoneWolf = false;
		}
	}

	// Get number of rounds to fire
	int RoundsShot = 1;
	if(Attacker->FireAllRounds[Attacker->AttackRequestType])
		RoundsShot = Attacker->GetWeaponAmmo();

	// Reduce ammo
	if(!GodMode)
		Attacker->ReduceAmmo(RoundsShot);

	// Weapon type specific code
	int WeaponType = WEAPON_MELEE;
	if(Attacker->AttackRequestType == WEAPONATTACK_MAIN)
		WeaponType = Attacker->MainWeaponType;

	// Play fire sound and generate fire/smoke particles
	if(WeaponType != WEAPON_MELEE) {
		_Hit Hit(HIT_NONE);
		GenerateHitEffects(Attacker, -1, Hit, false, Attacker->Rotation);
		if(Attacker->FireSoundTimer == 0.0) {
			if(Attacker->Type == _Object::PLAYER)
				ae::Audio.PlaySound(Attacker->GetSound(SOUND_FIRE, WEAPONATTACK_MAIN));
			else
				ae::Audio.PlaySound(Attacker->GetSound(SOUND_FIRE, WEAPONATTACK_MAIN), ae::_SoundSettings(glm::vec3(Attacker->Position.x, 0.0f, Attacker->Position.y), 1.0f, AUDIO_REFERENCE_DISTANCE, AUDIO_MAX_DISTANCE, AUDIO_ROLL_OFF));

			Attacker->FireSoundTimer = ENTITY_MAX_FIRESOUND_PERIOD;
		}
	}

	Attacker->StartTriggerDownAudio();

	// For each bullet that the weapon fires
	bool PlayedHitWallSound = false;
	_Entity *FirstHit = nullptr;
	std::vector<_Hit> Hits;
	Hits.reserve((size_t)Attacker->Penetration[Attacker->AttackRequestType]);
	int AttackCount = RoundsShot * Attacker->AttackCount[Attacker->AttackRequestType];
	for(int i = 0; i < AttackCount; i++) {
		Hits.clear();

		// Check for projectile weapons
		if(Attacker->Projectiles[Attacker->AttackRequestType]) {

			// Create projectile
			_Object *Projectile = Stats.CreateProjectile(*Attacker->Projectiles[Attacker->AttackRequestType], Attacker->Position);
			Projectile->ProjectileWeaponTemplate = Attacker->Weapons[Attacker->AttackRequestType];
			Projectile->ProjectileParticleTemplate = Attacker->GetParticle(PARTICLE_EXPLOSION);
			Projectile->GridTypes.reserve(2);
			if(Attacker->Type == _Object::PLAYER)
				Projectile->GridTypes.push_back(GRID_MONSTER);
			else
				Projectile->GridTypes.push_back(GRID_PLAYER);

			Projectile->Map = Map;
			Projectile->Owner = Attacker;
			Projectile->Rotation = Attacker->GenerateShotDirection();
			Projectile->Direction = glm::rotate(glm::vec2(0, -1), glm::radians(Projectile->Rotation));
			Projectile->Velocity = Projectile->Direction * Attacker->ProjectileSpeed[Attacker->AttackRequestType];
			Projectile->ProjectileMinDamage = Attacker->MinDamage[Attacker->AttackRequestType];
			Projectile->ProjectileMaxDamage = Attacker->MaxDamage[Attacker->AttackRequestType];
			Projectile->Depth = Attacker->Penetration[Attacker->AttackRequestType];
			Projectile->ProjectileCritChance = Attacker->CritChance[Attacker->AttackRequestType];
			Projectile->ProjectileCritDamage = Attacker->CritDamage[Attacker->AttackRequestType];
			Projectile->ProjectilePenetrationDamage = Attacker->PenetrationDamage[Attacker->AttackRequestType];
			Projectile->ProjectileExplosionSize = Attacker->ExplosionSize[Attacker->AttackRequestType];
			Projectile->ProjectileForce = Attacker->Force[Attacker->AttackRequestType];
			Projectile->Bounces = Attacker->StartingBounces[Attacker->AttackRequestType];
			if(Attacker->Type != _Object::PLAYER)
				Projectile->Color = Attacker->Color;
			if(!Projectile->LightTexture)
				Projectile->Color.a = std::max(PROJECTILE_MIN_ALPHA, Projectile->Color.a);
			if(Attacker->AttackWasSteady)
				Projectile->ProjectileCritChance *= PLAYER_STEADY_CRIT_FACTOR;

			Map->ObjectManager->AddObject(Projectile);
		}
		else {

			// Check weapon type
			glm::vec2 PushDirection;
			if(WeaponType == WEAPON_MELEE) {
				Map->CheckMeleeCollisions(Attacker, GridType, Attacker->Penetration[Attacker->AttackRequestType], Hits);
				PushDirection = Attacker->Direction;
			}
			else {

				// Check distance to the wall
				float ShotDirection = Attacker->GenerateShotDirection();
				PushDirection = glm::rotate(glm::vec2(0, -1), glm::radians(ShotDirection));
				Map->CheckBulletCollisions(Attacker, PushDirection, Hits, GridType, true, Attacker->Penetration[Attacker->AttackRequestType], _Tile::BULLET);

				// Generate tracer particle
				_ParticleTemplate *Template = &GameAssets.Particles["tracer0"];
				glm::vec2 ParticleStart = Attacker->Position + glm::rotate(glm::vec2(0, -Template->Size.y * 0.5f) + Attacker->WeaponOffset[Attacker->MainWeaponType], glm::radians(ShotDirection));
				_Particle *Tracer = new _Particle(_ParticleSpawn(Template, COLOR_WHITE, glm::vec2(0), ParticleStart, OBJECT_Z, ShotDirection));
				float Distance = glm::length(Hits.front().Position - Attacker->Position) - Template->Size.y;
				Tracer->Lifetime = Distance * Template->VelocityScale.y * GAME_FPS;
				Particles->Add(Tracer);
			}

			// Generate particle effects and reduce health
			float PenetrationDamage = 1.0f;
			for(const auto &Hit : Hits) {
				switch(Hit.Type) {
					case HIT_NONE:
					break;
					case HIT_WALL: {
						if(!PlayedHitWallSound) {
							ae::Audio.PlaySound(Attacker->GetSound(SOUND_RICOCHET, WEAPONATTACK_MAIN), ae::_SoundSettings(glm::vec3(Hit.Position.x, 0.0f, Hit.Position.y), 1.0f, AUDIO_REFERENCE_DISTANCE, AUDIO_MAX_DISTANCE, AUDIO_ROLL_OFF));
							PlayedHitWallSound = true;
						}

						bool CreateWallDecal = (Hit.Object && Hit.Object->Type == _Object::PROP) ? false : true;
						GenerateHitEffects(Attacker, HIT_WALL, Hit, false, Attacker->Rotation, CreateWallDecal);
					} break;
					case HIT_OBJECT: {
						_Entity *HitEntity = (_Entity *)Hit.Object;
						bool HitPlayer = Hit.Object->Type == _Object::PLAYER;

						// Keep track of first alive entity
						if(!HitPlayer && (!FirstHit || (FirstHit->Health <= 0 && HitEntity->Health > 0)))
							FirstHit = HitEntity;

						// Generate damage
						bool Crit = false;
						int64_t Damage = Attacker->GenerateDamage(Attacker->AttackRequestType, PenetrationDamage, Attacker->AttackWasSteady, Crit);
						Damage = HitEntity->ReduceDamage(Damage, false);
						if(GodMode && HitPlayer)
							Damage = 0;

						// Generate damage particles
						GenerateDamageText(Hit.Position, Damage, Crit, HitPlayer);

						// Update health
						HitEntity->UpdateHealth(-Damage);

						// Callback functions
						Attacker->OnAttack(HitEntity, Hit);
						HitEntity->OnHit(Attacker, Hit);

						// Apply penetration
						PenetrationDamage *= Attacker->PenetrationDamage[Attacker->AttackRequestType];

						// Add force
						HitEntity->ApplyForce(PushDirection, Attacker->Force[Attacker->AttackRequestType]);

						// Generate hit particles
						GenerateHitEffects(Attacker, HIT_OBJECT, Hit, !HitEntity->Health, Attacker->Rotation);
					} break;
				}
			}
		}
	}

	// Update HUD
	if(FirstHit)
		HUD->SetLastHit(FirstHit);

	// Update accuracy
	Attacker->ApplyRecoil();
}

// Handle pickup
void _PlayState::HandlePickup() {

	// Get nearby items
	std::unordered_map<_Object *, int> NearbyItems;
	_Object *ClosestObject = nullptr;
	Map->GetCloseObjects(Player->Position, Player->Radius, GRID_ITEM, NearbyItems, &ClosestObject);

	// Save closest item pointer
	ClosestItem = (_Item *)ClosestObject;
	for(auto &Iterator : NearbyItems) {
		_Item *NearbyItem = (_Item *)Iterator.first;
		if(!NearbyItem->Visible || NearbyItem->Filtered)
			continue;

		// Automatically pickup ammo/health
		if(NearbyItem->CanAutoPickup()) {
			if(!PickupObject(NearbyItem, Player->ApplyUse())) {
				IgnoreItems[NearbyItem] = 1;
				continue;
			}
		}
		// Manually pickup up an item
		else if(Player->ApplyUse() && Player->CanPickup())
			PickupObject(NearbyItem, true);
	}
}

// Called when the player dies
void _PlayState::PlayerDied() {

	// Clear message
	HUD->ClearMessageBox();

	// Dying sound
	ae::Audio.PlaySound(ae::Assets.Sounds["player_die0.ogg"], ae::_SoundSettings(glm::vec3(Player->Position.x, 0.0f, Player->Position.y), 1.0f, AUDIO_REFERENCE_DISTANCE, AUDIO_MAX_DISTANCE, AUDIO_ROLL_OFF));

	// Update monsters
	for(const auto &Entity : Monsters) {
		_Monster *Monster = (_Monster *)Entity;
		Monster->OnPlayerDeath();
	}
}

// End level
void _PlayState::EndLevel() {
	ae::Audio.StopSounds();
	Player->CancelReloading();

	Level = TouchingEndEvent->ItemID;
	bool GotOneHundredPercent = HUD->Kills[0] >= HUD->Kills[1] && HUD->Crates[0] >= HUD->Crates[1] && HUD->Secrets[0] >= HUD->Secrets[1];
	if(!GotOneHundredPercent)
		Player->Stat100Percent = false;

	// Update progression stats
	Player->ProgressionKills += HUD->Kills[0];
	Player->ProgressionCrates += HUD->Crates[0];
	Player->ProgressionSecrets += HUD->Secrets[0];

	// End of the game
	if(Level.empty()) {
		Menu.SetScoreStats(HUD, Player, true, Player->Progression + 1, GotOneHundredPercent);
		Level = Stats.FirstLevel;

		// Check achievements
		if(Player->Progression == 1) {
			if(Player->StatFistsOnly)
				Menu.UnlockAchievement("fists");

			if(Player->StatLoneWolf)
				Menu.UnlockAchievement("lonewolf");

			if(Player->TotalDeaths == 0 && Player->Hardcore && Player->PlayTime < ACHIEVEMENTS_BLEEDRUN_TIME)
				Menu.UnlockAchievement("bleedrun");
		}

		if(Player->Stat100Percent)
			Menu.UnlockAchievement("all");

		if(Player->Progression >= 5 && Player->Hardcore)
			Menu.UnlockAchievement("p5");

		if(Player->LavaTouches == 0)
			Menu.UnlockAchievement("smoked");

		// Reset stats
		Player->Progression = std::min(Player->Progression + 1, GAME_MAX_PROGRESSION);
		Player->ProgressionTime = 0;
		Player->ProgressionKills = 0;
		Player->ProgressionCrates = 0;
		Player->ProgressionSecrets = 0;
		Player->ProgressionDeaths = 0;
		Player->ResetAchievementTracking();
		Player->AddMissingBackpacks();
		Player->BuildFilterValues();
	}
	else {
		Menu.SetScoreStats(HUD, Player, false, 0, GotOneHundredPercent);
	}

	Player->CombatTimer = GAME_COMBAT_TIMER;
	Player->LevelTime = 0.0;
	Player->CheckpointIndex = TouchingEndEvent->Level;
	Player->MapID = Level;
	if(Map->MapType == MAPTYPE_CAMPAIGN)
		Player->Keys.clear();
	Save.SavePlayer(Player);

	NullState.LevelComplete = true;
	Framework.ChangeState(&NullState);
}

// Places an item into the player's inventory and returns the amount added
int _PlayState::PickupObject(_Item *Item, bool Manual) {
	if(!Item || !Item->Visible || !Item->Carry || (Manual && Item->CanHide() && ShowMoreInfo()))
		return 0;

	// Attempt to add item
	int AmountAdded = 0;
	int AddResult = Player->AddItem(Item, AmountAdded, Manual);
	if(AddResult) {
		if(Item == ClosestItem)
			ClosestItem = nullptr;

		// Set up particles
		if(Item->CanAutoPickup() && AmountAdded) {

			// Initialize
			std::ostringstream Buffer;
			Buffer.imbue(std::locale(Config.Locale));
			glm::vec4 ParticleColor = COLOR_WHITE;
			switch(Item->Type) {
				case _Object::AMMO:
					Buffer << "+" << AmountAdded;
				break;
				case _Object::CONSUMABLE:
					Item->GetConsumableParticleText(Buffer, AmountAdded);
					ParticleColor = COLOR_GREEN;
				break;
				case _Object::KEY:
					Buffer << "+" << Item->Name;
					Save.SavePlayer(Player);
				break;
			}

			// Add particle
			GenerateTextParticle(Player->Position - glm::vec2(0.0f, 0.5f), Buffer.str(), ParticleColor);
		}

		// Remove item from map
		if(AddResult != ADD_QUIETFULL)
			Map->RemoveObjectFromGrid(Item, GRID_ITEM);

		// Handle deletion
		if(AddResult == ADD_REMOVE) {
			Map->ObjectManager->RemoveObject(Item);
		}
		else if(AddResult == ADD_DELETE) {
			Item->Active = false;
			CursorItemTimer = 0;
		}

		// Reset use timer
		if(Manual && !Item->CanAutoPickup())
			Player->UseTimer = 0.0;
	}
	else
		HUD->ShowTextMessage(HUD_INVENTORYFULLMESSAGE, HUD_INVENTORYFULLTIME);

	return AmountAdded;
}

// Determine if more info should be shown
bool _PlayState::ShowMoreInfo() {
	return ae::Input.ModKeyDown(KMOD_ALT) || ae::Actions.State[Action::GAME_MOREINFO].Value > 0.0f;
}

// Determine if minimap icons should be shown
bool _PlayState::DrawMinimapIcons() {
	return Config.ShowIcons ^ ShowMoreInfo();
}

// Determine if HoverItem should be set to CursorItem
bool _PlayState::SetHoverItem() {

	if(!CursorItem)
		return false;

	if(ae::Graphics.Element->HitElement)
		return false;

	if(ShowMoreInfo() && CursorItem->CanHide())
		return false;

	if(HUD->HoverItem && ClosestItem != HUD->HoverItem)
		return false;

	if(ShowMoreInfo() || HUD->InventoryOpen || CursorItemTimer > HUD_CURSOR_ITEM_WAIT || ClosestItemTimer >= HUD_STANDOVER_TIME)
		return true;

	return false;
}

// Update filter icon text
void _PlayState::SetFilterTextValue(ae::_Element *Element, int Value) {
	if(Value == ITEM_QUALITY_MIN)
		Element->Text = "ALL";
	else
		Element->Text = std::to_string(Value) + "+";

	// Set font size
	Element->Font = (Value >= 100) ? ae::Assets.Fonts["hud_tiny"] : ae::Assets.Fonts["hud_char"];
}

// Change filter level for a filter type
void _PlayState::ChangeFilterLevel(int FilterType) {

	// Toggle between all and saved filter
	if(ae::Input.ModKeyDown(KMOD_CTRL)) {
		if(Player->Filters[FilterType] == 0 && Player->LastFilters[FilterType] != -1) {
			Player->Filters[FilterType] = Player->LastFilters[FilterType];
		}
		else {
			Player->LastFilters[FilterType] = Player->Filters[FilterType];
			Player->Filters[FilterType] = 0;
		}
	}
	else {

		// Get max
		int Max = (int)Player->FilterValues[FilterType].size();

		// Update filter
		Player->Filters[FilterType] += ae::Input.ModKeyDown(KMOD_SHIFT) ? -1 : 1;
		if(Player->Filters[FilterType] >= Max)
			Player->Filters[FilterType] = 0;
		else if(Player->Filters[FilterType] < 0)
			Player->Filters[FilterType] = Max - 1;

		// Disable toggle when set to all
		if(Player->Filters[FilterType] == 0)
			Player->LastFilters[FilterType] = -1;
	}

	// Update HUD
	std::string Message;
	switch(FilterType) {
		case FILTER_GEAR:
			SetFilterTextValue(ae::Assets.Elements["label_hud_filters_gear"], Player->GetFilterValue(FilterType));
			Message = std::string("GEAR QUALITY FILTER: ") + ae::Assets.Elements["label_hud_filters_gear"]->Text;
		break;
		case FILTER_MODS:
			SetFilterTextValue(ae::Assets.Elements["label_hud_filters_mods"], Player->GetFilterValue(FilterType));
			Message = std::string("MOD QUALITY FILTER: ") + ae::Assets.Elements["label_hud_filters_mods"]->Text;
		break;
	}

	HUD->ShowTextMessage(Message, 1, true);
}

// Open door or handle switches, return true on success
void _PlayState::ActivateEvent() {

	// Open a door if possible
	glm::ivec2 Position;
	Map->GetAdjacentTile(Player->Position, Player->Rotation, Position);

	// Check for events
	std::vector<_Event *> &Events = Map->GetEventList(Position);
	for(auto Event : Events) {
		if(!Event->Active)
			continue;

		// Check for doors or switches
		if(Event->Type != EVENT_DOOR && Event->Type != EVENT_WALLSWITCH)
			continue;

		// Check for key in inventory and use it
		if(!Event->ItemID.empty()) {
			if(Player->Keys.find(Event->ItemID) == Player->Keys.end()) {
				HUD->ShowMessageBox("You need the " + Stats.Objects.at(Event->ItemID).Name, HUD_KEY_MESSAGETIME, UI_MESSAGE_SMALL_SIZE);
				if(!LockedSound || (LockedSound && !LockedSound->IsPlaying()))
					LockedSound = ae::Audio.PlaySound(ae::Assets.Sounds["game_locked.ogg"]);

				Player->UseTimer = 0.0;
				return;
			}

			std::string KeyName = Stats.Objects.at(Event->ItemID).Name;
			std::transform(KeyName.begin(), KeyName.end(), KeyName.begin(), ::toupper);
			HUD->ShowTextMessage(KeyName + " USED", 2.0f);
		}

		// Change map
		Map->ChangeMapState(Event);

		// Play sound
		if(!Event->SoundID.empty())
			ae::Audio.PlaySound(ae::Assets.Sounds[Event->SoundID]);

		// Decrement level
		if(Event->Level > 0) {
			Event->Decrement();
			if(Event->Level == 0)
				Event->Active = false;
		}

		// Toggle event
		Event->Switched = !Event->Switched;

		// Reset player's use timer
		Player->UseTimer = 0.0;
	}
}

// Creates a random item from an entity
void _PlayState::CreateItemDrop(const _Entity *Entity, float DropRate) {
	if(Entity->Type != _Object::MONSTER)
		return;

	// Get monster
	const _Monster *Monster = (const _Monster *)Entity;
	if(!Monster->ItemDrop)
		return;

	// Roll for items
	int Rolls;
	for(int i = 0; i < Monster->Template.Attributes.at("drop_count").Int; i++) {

		// Bosses only drop one item
		int AddedRarityChance = Monster->Quality;
		if(Monster->Template.IsBoss) {
			Rolls = 1;
		}
		else {
			Rolls = (int)DropRate;

			// Check for extra roll
			double MultiOdds = DropRate - (int)DropRate;
			double MultiRoll = ae::GetRandomReal(0, 1);
			if(MultiRoll <= MultiOdds)
				Rolls++;

			// Add unique rolls
			if(Monster->Unique)
				Rolls += Monster->Unique->Rolls;
		}

		// Roll for each item
		for(int j = 0; j < Rolls; j++) {

			// Roll for drop
			_ObjectSpawn ObjectSpawn;
			Stats.GetRandomDrop(Monster->ItemDrop, &ObjectSpawn);
			if(!ObjectSpawn.Type)
				continue;

			// Spawn object on player if item can't be reached
			if(!Map->CanPass(Map->GetValidCoord(Monster->Position), _Tile::ENTITY) || Monster->Template.IsBoss)
				ObjectSpawn.Position = Player->Position;
			else
				ObjectSpawn.Position = Monster->Position;

			// Adjust position
			if(!Monster->Template.IsBoss)
				ObjectSpawn.Position = Map->FindSuitableItemPosition(ObjectSpawn.Position, ObjectSpawn.Type, ITEM_RADIUS, ITEM_PLACEMENT_ATTEMPTS);

			ObjectSpawn.Level = Monster->Level;
			SpawnObject(&ObjectSpawn, true, 0, AddedRarityChance);
		}
	}
}

// Drop an item on the floor
void _PlayState::DropItem(_Item *Item) {
	Item->SetPosition(Map->FindSuitableItemPosition(Player->Position, Item->Type, ITEM_RADIUS, ITEM_PLACEMENT_ATTEMPTS));
	Map->AddObject(Item, GRID_ITEM);
}

// Updates the monsters
void _PlayState::UpdateMonsters(double FrameTime) {
	bool HasBossKey = Player->Keys.find("key_boss") != Player->Keys.end();

	// Loop through monsters
	ActiveAI = 0;
	for(auto MonsterIterator = Monsters.begin(); MonsterIterator != Monsters.end();) {
		_Monster *Monster = (_Monster *)*MonsterIterator;

		if(!Monster->Active) {
			Map->RemoveObjectFromGrid(Monster, GRID_MONSTER);
			delete Monster;
			MonsterIterator = Monsters.erase(MonsterIterator);
		}
		else {
			Monster->Update(FrameTime);
			if(Monster->MoveState)
				ActiveAI++;

			// Add to minimap
			if((Monster->ShowOnMinimap() || HasBossKey) && Monster->Health > 0 && Map->CheckMinimapBounds(Monster->Bounds)) {
				_MinimapIcon MinimapIcon;
				MinimapIcon.Bounds = Monster->Bounds;
				if(Monster->IsCrate())
					Map->MinimapIcons[_Map::MINIMAP_CRATE].push_back(MinimapIcon);
				else
					Map->MinimapIcons[_Map::MINIMAP_MONSTER].push_back(MinimapIcon);
			}

			// Attack
			if(Monster->AttackMade) {
				Monster->FacePosition(Player->Position);
				ResolveAttack(Monster, GRID_PLAYER);
			}

			// Add to render list
			if(Camera->IsAABBInView(Monster->Bounds)) {
				int RenderListType = Monster->IsCrate() ? _ObjectManager::RENDER_PROP : _ObjectManager::RENDER_MONSTER;
				Map->ObjectManager->RenderList[RenderListType].push_back(Monster);
			}

			// Check light bounds
			if(Monster->LightTexture && Camera->IsAABBInView(Monster->LightBounds))
				Map->ObjectManager->RenderList[_ObjectManager::RENDER_LIGHTS].push_back(Monster);

			++MonsterIterator;
		}
	}
}

// Checks for the player triggering events
void _PlayState::CheckEvents(const _Entity *Entity) {
	Player->TileChanged = false;
	PreviousTouchingEndEvent = TouchingEndEvent;
	TouchingEndEvent = nullptr;

	// Check for events triggered by walking
	glm::ivec2 Position = Map->GetValidCoord(Entity->Position);
	std::vector<_Event *> &Events = Map->GetEventList(Position);
	for(auto Event : Events) {
		if(!Event->Active)
			continue;

		// Perform action
		switch(Event->Type) {
			case EVENT_SPAWN:
				Event->StartTimer();
				ActiveEvents.push_back(Event);
				Event->Active = false;
			break;
			case EVENT_CHECK:
				switch(Map->MapType) {
					case MAPTYPE_CAMPAIGN:
						if(Event->Level > Player->CheckpointIndex) {
							Player->CheckpointIndex = Event->Level;
							HUD->ShowTextMessage(HUD_CHECKPOINTMESSAGE, HUD_CHECKPOINTTIME);
							Save.SavePlayer(Player);
						}
						Event->Active = false;
					break;
					case MAPTYPE_ADVENTURE:
						if(Event->Level != Player->CheckpointIndex) {
							Player->CheckpointIndex = Event->Level;
							HUD->ShowTextMessage(HUD_CHECKPOINTMESSAGE, HUD_CHECKPOINTTIME);
							Save.SavePlayer(Player);
						}
					break;
					default:
					break;
				}
			break;
			case EVENT_END: {
				TouchingEndEvent = Event;
			} break;
			case EVENT_TEXT: {

				// Check for tutorial messages
				bool IsTutorial = Event->ItemID.find("tutorial_") == 0;
				if(!IsTutorial || (Config.Tutorial && Player->Progression == 1)) {
					if(IsTutorial)
						ae::Audio.PlaySound(ae::Assets.Sounds["game_message0.ogg"]);
					HUD->ShowMessageBox(Stats.TransformedText[Event->ItemID], Event->ActivationPeriod, UI_MESSAGE_SIZE);
				}

				if(Event->Level != 0)
					Event->Active = false;
			} break;
			case EVENT_SOUND:
				if(ae::Assets.Sounds[Event->SoundID]) {
					Event->StartTimer();
					ActiveEvents.push_back(Event);
				}
				Event->Active = false;
			break;
			case EVENT_FLOORSWITCH:
			case EVENT_ENABLE:
				Event->StartTimer();
				ActiveEvents.push_back(Event);
				Event->Active = false;
			break;
			case EVENT_TELEPORT: {
				if(Event->Level > 0) {
					Event->Decrement();
					if(Event->Level == 0)
						Event->Active = false;
				}

				// Play sound
				if(!Event->SoundID.empty())
					ae::Audio.PlaySound(ae::Assets.Sounds[Event->SoundID]);

				if(Event->Tiles.size() > 0) {
					size_t TileID = ae::GetRandomInt<size_t>(0, Event->Tiles.size()-1);
					glm::vec2 NewPosition(Event->Tiles[TileID].Coord.x + 0.5f, Event->Tiles[TileID].Coord.y + 0.5f);
					Particles->Create(_ParticleSpawn(GameAssets.GetParticleTemplate(Event->ParticleID), COLOR_WHITE, glm::vec2(0), NewPosition, OBJECT_Z, 0));
					Player->WarpPosition(NewPosition);
				}
			} break;
			case EVENT_LIGHT: {
				if(Event->Level > 0) {
					Event->Active = false;
					Event->StartTimer();
					ActiveEvents.push_back(Event);
				}
				else
					Map->SetAmbientLight(Event->ItemID);
			} break;
			case EVENT_SECRET: {
				ae::Audio.PlaySound(ae::Assets.Sounds["game_secret0.ogg"]);
				HUD->ShowMessageBox("You have found a secret!", HUD_SECRET_MESSAGETIME, UI_MESSAGE_SMALL_SIZE);
				HUD->Secrets[0]++;
				Event->Active = false;
			} break;
			case EVENT_LAVA: {
				ae::Audio.PlaySound(ae::Assets.Sounds["game_lava0.ogg"]);
				int64_t Damage = Stats.Progressions[(size_t)Player->Progression].LavaDamage;
				Player->UpdateHealth(-Damage);
				GenerateDamageText(Player->Position, Damage, false, true);
				Particles->Create(_ParticleSpawn(GameAssets.GetParticleTemplate(Event->ParticleID), COLOR_WHITE, glm::vec2(0), Player->Position, OBJECT_Z, 0));
				Player->LavaTouches++;
			} break;
			default:
			break;
		}
	}

	// Remove end of level message
	if(PreviousTouchingEndEvent && !TouchingEndEvent)
		HUD->ClearMessageBox(1.0);
}

// Update the active events
void _PlayState::UpdateEvents(double FrameTime) {

	// Activate events
	for(auto ActiveEventIterator = ActiveEvents.begin(); ActiveEventIterator != ActiveEvents.end(); ++ActiveEventIterator) {
		_Event *Event = *ActiveEventIterator;
		Event->Update(FrameTime);
		if(!Event->TimerExpired())
			continue;

		glm::vec2 Position;
		bool Decrement = false;
		switch(Event->Type) {
			case EVENT_SPAWN: {
				const std::vector<_EventTile> &Tiles = Event->Tiles;
				for(size_t i = 0; i < Tiles.size(); i++) {
					Position.x = Tiles[i].Coord.x + 0.5f;
					Position.y = Tiles[i].Coord.y + 0.5f;

					// Randomize offset
					if(Event->IsMonsterSpawn && Event->SpawnMultiplier) {
						Position.x += ae::GetRandomReal(-0.25, 0.25);
						Position.y += ae::GetRandomReal(-0.25, 0.25);
					}

					// Spawn monster
					if(Event->MonsterID.size()) {

						// Chance for special monster
						size_t SpecialType = 0;
						if(!Event->IsBossSpawn && Player->Progression > 1 && ae::GetRandomInt<int>(1, 100) <= Stats.Progressions[(size_t)Player->Progression].SpecialChance)
							SpecialType = ae::GetRandomInt<size_t>(1, Stats.Specials.size() - 1);

						// Spawn monsters
						for(int j = 0; j < Event->SpawnMultiplier; j++) {
							_Monster *Monster = Stats.CreateMonster(Event->MonsterID, Position, Event->SpawnLevel + Map->GetAddedLevel(), Map->Progression, SpecialType, !Event->IsBossSpawn, Player->RarityChance);
							Monster->Player = Player;
							Monster->FreePathingTimer = ENTITY_FREEPATHING_TIMER_INCREMENT * j;
							AddMonster(Monster);
						}
					}

					// Spawn item
					if(Event->ItemID.size())
						Map->AddObject(Stats.CreateItem(Event->ItemID, Position, Event->SpawnLevel + Map->GetAddedLevel(), 0, true, Player->RarityChance, Player->Progression), GRID_ITEM);

					// Spawn particles
					if(Event->ParticleID.size())
						Particles->Create(_ParticleSpawn(GameAssets.GetParticleTemplate(Event->ParticleID), COLOR_WHITE, glm::vec2(0), Position, OBJECT_Z, 0));
				}

				Decrement = true;
			} break;
			case EVENT_SOUND: {
				const std::vector<_EventTile> &Tiles = Event->Tiles;
				if(Tiles.size()) {
					for(size_t i = 0; i < Tiles.size(); i++)
						ae::Audio.PlaySound(ae::Assets.Sounds[Event->SoundID], ae::_SoundSettings(glm::vec3(Tiles[i].Coord.x, 0, Tiles[i].Coord.y), 1.0f, AUDIO_REFERENCE_DISTANCE, AUDIO_MAX_DISTANCE, AUDIO_ROLL_OFF));
				}
				else
					ae::Audio.PlaySound(ae::Assets.Sounds[Event->SoundID]);

				Decrement = true;
			} break;
			case EVENT_FLOORSWITCH:
				Map->ChangeMapState(Event);
				Decrement = true;
			break;
			case EVENT_ENABLE: {
				const std::vector<_EventTile> &Tiles = Event->Tiles;
				for(size_t i = 0; i < Tiles.size(); i++)
					Map->ToggleEventActive((size_t)Tiles[i].BlockID);

				Decrement = true;
			} break;
			case EVENT_LIGHT: {
				Map->SetAmbientLight(Event->ItemID);
				Decrement = true;
			} break;
			default:
			break;
		}

		// Decrease the event level
		if(Decrement) {

			// Play sound for other events
			if(Event->Type != EVENT_SOUND && !Event->SoundID.empty())
				ae::Audio.PlaySound(ae::Assets.Sounds[Event->SoundID]);

			Event->StartTimer();
			if(Event->Level != -1)
				Event->Decrement();

			if(Event->Level == 0) {
				ActiveEventIterator = ActiveEvents.erase(ActiveEventIterator);
				if(ActiveEventIterator == ActiveEvents.end())
					break;
			}
		}
	}
}

// Deletes the monsters
void _PlayState::DeleteMonsters() {
	for(auto Iterator : Monsters) {
		if(Iterator)
			delete Iterator;
	}

	Monsters.clear();
}

// Spawn an object in the map
void _PlayState::SpawnObject(const _ObjectSpawn *ObjectSpawn, bool GenerateStats, int AddedLevel, int AddedRarityChance) {
	if(ObjectSpawn->Type == _Object::MONSTER) {
		_Monster *Monster = Stats.CreateMonster(ObjectSpawn->ID, ObjectSpawn->Position, ObjectSpawn->Level + Map->GetAddedLevel(), Map->Progression, 0, GenerateStats, Player->RarityChance + AddedRarityChance);
		Monster->Player = Player;
		AddMonster(Monster);
		Map->TotalExperience += Monster->ExperienceGiven;
		if(Monster->IsCrate())
			Map->Crates++;
		else
			Map->Monsters++;
	}
	else if(ObjectSpawn->Type == _Object::PROP)
		Map->AddObject(Stats.CreateProp(ObjectSpawn->ID, ObjectSpawn->Position, ObjectSpawn->Rotation, ObjectSpawn->Scale), GRID_MONSTER);
	else
		Map->AddObject(Stats.CreateItem(ObjectSpawn->ID, ObjectSpawn->Position, ObjectSpawn->Level + AddedLevel, 0, GenerateStats, Player->RarityChance + AddedRarityChance, Player->Progression), GRID_ITEM);
}

// Adds a monster to the monster list and collision grid
void _PlayState::AddMonster(_Monster *Monster) {
	Monster->Map = Map;
	if(Map->SimpleAI)
		Monster->AIType = AI_SIMPLE;
	Monsters.push_back(Monster);
	Map->AddObjectToGrid(Monster, GRID_MONSTER);
}

// Removes a monster from object list and collision grid
void _PlayState::RemoveMonster(_Monster *Monster) {
	for(auto Iterator = Monsters.begin(); Iterator != Monsters.end(); ++Iterator) {
		if(*Iterator == Monster) {
			Monsters.erase(Iterator);
			break;
		}
	}

	Map->RemoveObjectFromGrid(Monster, GRID_MONSTER);
}

// Generate particles depending on hit type
void _PlayState::GenerateHitEffects(_Entity *Attacker, const int Type, const _Hit &Hit, bool Death, float Rotation, bool CreateWallDecal) {
	if(Attacker && Type == -1) {
		glm::vec2 ParticlePosition = Attacker->Position + glm::rotate(Attacker->WeaponOffset[Attacker->MainWeaponType], glm::radians(Rotation));
		Particles->Create(_ParticleSpawn(Attacker->GetParticle(PARTICLE_FIRE), COLOR_WHITE, glm::vec2(0), ParticlePosition, OBJECT_Z, Rotation));
		Particles->Create(_ParticleSpawn(Attacker->GetParticle(PARTICLE_SMOKE), COLOR_WHITE, glm::vec2(0), ParticlePosition, OBJECT_Z, 0));
	}
	else if(Attacker && Type == HIT_WALL) {
		Particles->Create(_ParticleSpawn(Attacker->GetParticle(PARTICLE_RICOCHET), COLOR_WHITE, Hit.Normal, Hit.Position, OBJECT_Z, Rotation));
		if(CreateWallDecal && Config.WallDecals)
			Particles->Create(_ParticleSpawn(Attacker->GetParticle(PARTICLE_BULLETHOLE), COLOR_WHITE, Hit.Normal, Hit.Position, OBJECT_Z, Rotation));
	}
	else if(Type == HIT_OBJECT) {
		glm::vec2 ParticlePosition = _Map::GenerateRandomPointInCircle(0.2f) + Hit.Object->Position;
		Particles->Create(_ParticleSpawn(Hit.Object->GetParticle(PARTICLE_HIT), COLOR_WHITE, Hit.Normal, Hit.Position, OBJECT_Z, Rotation));
		if(Death && Config.FloorDecals)
			Particles->Create(_ParticleSpawn(Hit.Object->GetParticle(PARTICLE_FLOORDECAL), COLOR_WHITE, Hit.Normal, ParticlePosition, ITEM_Z, Rotation));
	}
}

// Create damage number particles
void _PlayState::GenerateDamageText(const glm::vec2 &Position, int64_t Value, bool Crit, bool HitPlayer) {

	// Get text
	std::ostringstream Buffer;
	Buffer.imbue(std::locale(Config.Locale));
	Buffer << Value;

	_Particle *DamageParticle = GenerateTextParticle(Position + _Map::GenerateRandomPointInCircle(0.2f), Buffer.str());

	// Set color
	if(Crit)
		DamageParticle->Color = COLOR_YELLOW;
	else if(HitPlayer)
		DamageParticle->Color = COLOR_RED;
}

// Generate text particle
_Particle *_PlayState::GenerateTextParticle(const glm::vec2 &Position, const std::string &Value, const glm::vec4 &Color) {

	// Create particle
	_Particle *Particle = new _Particle(_ParticleSpawn(GameAssets.GetParticleTemplate("text0"), Color, glm::vec2(0), Position, OBJECT_Z, 0));
	Particle->Text = Value;
	Particles->Add(Particle);

	return Particle;
}

// Generate explosion particles
void _PlayState::GenerateExplosion(const _ParticleTemplate *ParticleTemplate, const glm::vec2 &Position, const glm::vec2 &Scale) {
	if(!Particles->Create(_ParticleSpawn(ParticleTemplate, COLOR_WHITE, glm::vec2(0), Position, OBJECT_Z, 0)))
		return;

	// Set scale
	_Particle *Particle = Particles->Particles.back();
	Particle->Scale = Scale;
}

// Generate projectile particle effects
void _PlayState::GenerateProjectileEffects(const _ParticleTemplate *ParticleTemplate, const glm::vec2 &Position, const glm::vec4 &Color) {
	Particles->Create(_ParticleSpawn(ParticleTemplate, Color, glm::vec2(0), Position, OBJECT_Z, 0));
}

// Determine if game is paused
bool _PlayState::IsPaused() {
	return Menu.GetState() != _Menu::STATE_NONE;
}

// Set minimap capture area
void _PlayState::SetMinimapCaptureSize() {

	// Full size map
	if(ae::Actions.State[Action::GAME_MAP].Value > 0.0f) {
		Map->MinimapCaptureSize = MINIMAP_FULL_CAPTURE_SIZE;
		Map->MinimapCaptureSize.x *= ae::Graphics.AspectRatio;
	}
	else
		Map->MinimapCaptureSize = Map->MinimapSizes[(size_t)Player->MinimapSizeIndex].Capture;
}
