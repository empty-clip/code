/******************************************************************************
* Empty Clip
* Copyright (C) 2025 Alan Witkowski
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/
#pragma once

#include <glm/vec4.hpp>

const glm::vec4 COLOR_WHITE           = glm::vec4(1.0f,   1.0f,   1.0f,    1.0f);
const glm::vec4 COLOR_TWHITE          = glm::vec4(1.0f,   1.0f,   1.0f,    0.5f);
const glm::vec4 COLOR_DARK            = glm::vec4(0.3f,   0.3f,   0.3f,    1.0f);
const glm::vec4 COLOR_GRAY            = glm::vec4(0.6f,   0.6f,   0.6f,    1.0f);
const glm::vec4 COLOR_TGRAY           = glm::vec4(1.0f,   1.0f,   1.0f,    0.2f);
const glm::vec4 COLOR_RED             = glm::vec4(1.0f,   0.0f,   0.0f,    1.0f);
const glm::vec4 COLOR_GREEN           = glm::vec4(0.0f,   1.0f,   0.0f,    1.0f);
const glm::vec4 COLOR_BLUE            = glm::vec4(0.0f,   0.0f,   1.0f,    1.0f);
const glm::vec4 COLOR_YELLOW          = glm::vec4(1.0f,   1.0f,   0.0f,    1.0f);
const glm::vec4 COLOR_MAGENTA         = glm::vec4(1.0f,   0.0f,   1.0f,    1.0f);
const glm::vec4 COLOR_CYAN            = glm::vec4(0.0f,   1.0f,   1.0f,    1.0f);
const glm::vec4 COLOR_GOLD            = glm::vec4(0.76f,  0.73f,  0.173f,  1.0f);
const glm::vec4 COLOR_FAINT_WHITE     = glm::vec4(1.0f,   1.0f,   1.0f,    0.7f);
const glm::vec4 COLOR_FAINT_GOLD      = glm::vec4(0.76f,  0.73f,  0.173f,  0.7f);
